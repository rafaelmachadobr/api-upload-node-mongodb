const fs = require("fs");
const Picture = require("../models/Picture");

exports.create = async (req, res) => {
  try {
    const { name } = req.body;
    const file = req.file;

    const picture = new Picture({
      name,
      src: file.path,
    });

    await picture.save();
    res.status(201).json({ picture, message: "Image saved successfully!" });
  } catch (err) {
    res.status(500).json({ message: "Error saving image" });
  }
};

exports.findAll = async (_req, res) => {
  try {
    const pictures = await Picture.find();

    res.json(pictures);
  } catch (err) {
    res.status(500).json({ message: "Error retrieving images" });
  }
};

exports.remove = async (req, res) => {
  try {
    const picture = await Picture.findById(req.params.id);

    if (!picture) {
      return res.status(404).json({ message: "Image not found" });
    }

    fs.unlinkSync(picture.src);

    await picture.deleteOne();

    res.json({ message: "Image removed successfully!" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: "Error removing image" });
  }
};
