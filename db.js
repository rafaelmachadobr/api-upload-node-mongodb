const mongoose = require("mongoose");
require("dotenv").config();

mongoose.set("strictQuery", true);

main().catch((err) => console.log(err));

async function main() {
  const mongoUser = process.env.MONGO_INITDB_ROOT_USERNAME;
  const mongoPassword = process.env.MONGO_INITDB_ROOT_PASSWORD;
  const mongoHost = process.env.MONGO_HOST;
  const mongoPort = process.env.MONGO_PORT;

  await mongoose.connect(
    `mongodb://${mongoUser}:${mongoPassword}@${mongoHost}:${mongoPort}/?retryWrites=true&w=majority`
  );

  console.log("Connected to MongoDB");
}

module.exports = mongoose;
